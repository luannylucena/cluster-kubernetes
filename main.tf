terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.30.0"
    }
  }
}

provider "google" {
  # Configuration options
   project = "bootcamp-avanti-424814"
   region = "us-central1"
   credentials = "key.json"
}