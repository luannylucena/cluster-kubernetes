# Projeto de Deploy de Banco de Dados com Docker, Kubernetes, GitLab CI/CD e GCP

## Descrição

Este projeto demonstra como configurar e implantar um banco de dados MySQL em um cluster Kubernetes utilizando Docker, GitLab CI/CD e Google Cloud Platform (GCP). Inclui arquivos de configuração para Terraform, scripts de deployment para Kubernetes e configuração de pipeline no GitLab CI/CD.

## Índice
- [Instalação](#instalação)
- [Configuração](#configuração)
- [Uso](#uso)
- [Arquivos Incluídos](#arquivos-incluídos)
- [Contribuição](#contribuição)
- [Licença](#licença)

## Instalação

### Pré-requisitos

- Docker
- Kubernetes
- gcloud (Google Cloud SDK)
- GitLab Runner
- Terraform

### Passos

1. Clone o repositório:
   
    - `git clone https://gitlab.com/usuario/projeto.git`
    - `cd projeto`

2. Configure o Docker e o Kubernetes:
    
    - `./script.sh`

3. Configure o Terraform para criar os recursos no GCP:
   
    - `terraform init`
    - `terraform apply`

## Configuração

### GitLab CI/CD

1. No GitLab, adicione as variáveis de ambiente necessárias:

- `user`: Usuário do Docker Hub
- `password`: Senha do Docker Hub
- `GCP_PROJECT`: ID do projeto no GCP
- `GCP_REGION`: Região onde os recursos serão criados
- `GCP_CREDENTIALS`: Credenciais do GCP em formato JSON

2. Configure o pipeline do GitLab com o conteúdo em .gitlab-ci.yml

## Uso

1. Execute o pipeline no GitLab para construir as imagens Docker e implantá-las no Kubernetes.

2. Verifique os serviços rodando no cluster:
   
   - `kubectl get services`

## Arquivos Incluídos

- `.gitlab-ci.yml`: Pipeline de CI/CD do GitLab.
- `app.yml`: Deployment da aplicação no Kubernetes.
- `mysql.yml`: Deployment do banco de dados MySQL no Kubernetes.
- `load-balancer.yml`: Serviço de balanceador de carga para a aplicação.
- `script.sh`: Script de instalação de dependências.
- `Dockerfile`: Arquivo de configuração para a construção da imagem Docker.
- `terraform/`: Configurações do Terraform para criar recursos no GCP.

## Contribuição

1. Faça um fork do projeto.
2. Crie uma nova branch (`git checkout -b feature/nova-feature`).
3. Faça commit das suas mudanças (`git commit -am 'Adicionei uma nova feature'`).
4. Faça push para a branch (`git push origin feature/nova-feature`).
5. Abra um Pull Request.
