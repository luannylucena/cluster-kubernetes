#!/bin/bash

#Atualização
sudo apt-get update 
sudo apt-get upgrade -y

#Instalação do Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

#Instalação kubectl
sudo apt-get install kubectl -y
sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin -y

#Instação Gitlab runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

#Permissão para acessar o Docker
sudo /usr/sbin/usermod -aG docker gitlab-runner 

#Vincular a VM com o repositório Git
sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--token glrt-F21jwzCzvLLRAdyXmh7D \
--executor shell \